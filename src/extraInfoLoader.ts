import axios from 'axios';

import storage from '@/storage';
import logger from '@/logger';
import { ExtraInfoModel } from '@/interfaces';

interface ICache {
  [key: string]: ExtraInfoModel;
}

class ExtraInfoLoader {
  private fromLocationRegex = /Toodud riigist: (?:<b>)?([a-zõäöü]+)(?:<\/b>)?/i;
  private vehicleLocationRegex = /Sõiduki asukoht: (?:<b>)?([a-zõäöü-]+)(?:<\/b>)?/i;

  public async get(id: string) {
    const cache = await storage.get<ICache>('cache');
    if (cache && cache[id]) {
      logger.log(`Loading extrainfo for "${id}" from cache`, 'CACHE', 'green');
      return cache[id];
    } else {
      logger.log(`Loading extrainfo for "${id}" from http://www.auto24.ee/used/${id}`, 'URL', 'red');

      const extraInfo: ExtraInfoModel = {};

      const response = await axios.get(`https://www.auto24.ee/used/${id}`);

      const parser = new DOMParser();
      const wrapper = parser.parseFromString(response.data, 'text/html');

      const otherInfo = wrapper.querySelector<HTMLDivElement>('div.section.other-info');
      if (!otherInfo) return extraInfo;

      const fromLocation = otherInfo.innerHTML.match(this.fromLocationRegex);
      const vehicleLocation = otherInfo.innerHTML.match(this.vehicleLocationRegex);

      if (fromLocation) {
        extraInfo.originLocation = fromLocation[1];
      }

      if (vehicleLocation) {
        extraInfo.currentLocation = vehicleLocation[1];
      }

      await this.setWithCache(id, extraInfo, cache || {});

      return extraInfo;
    }
  }

  private async setWithCache(id: string, extraInfo: ExtraInfoModel, cache: ICache) {
    cache[id] = extraInfo;
    await storage.set(cache, 'cache');
  }
}

export default new ExtraInfoLoader();
