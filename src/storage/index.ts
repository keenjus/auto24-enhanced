import logger from '@/logger';

class Storage {
  public get<T>(key: string) {
    return new Promise<T | null>((resolve, reject) => {
      try {
        chrome.storage.local.get([key], async items => {
          resolve(items[key]);
        });
      } catch (err) {
        logger.error(err);
        reject(err);
      }
    });
  }

  public set<T>(data: T, key: string) {
    return new Promise((resolve, reject) => {
      try {
        chrome.storage.local.set({ [key]: data }, () => {
          resolve();
        });
      } catch (err) {
        logger.error(err);
        reject(err);
      }
    });
  }
}

export default new Storage();
