export interface OptionsModel {
  enableColoring: boolean;
}

export interface ExtraInfoModel {
  currentLocation?: string;
  originLocation?: string;
}
