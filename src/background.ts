import storage from './storage';
import logger from './logger';

function setBrowserActionIcon(initialized: boolean) {
  if (initialized) {
    chrome.browserAction.setIcon({ path: 'assets/icon16.png' });
  } else {
    chrome.browserAction.setIcon({ path: 'assets/icon16_grayscale.png' });
  }
}

async function isEnabled() {
  const initialized = await storage.get<boolean>('enabled');
  // If first load then return true
  if (typeof initialized !== 'boolean') {
    await storage.set(true, 'enabled');
    return true;
  }
  return initialized;
}

// NB! When dealing with asynchronous responses we have to return "true" first
chrome.runtime.onMessage.addListener((request, _, sendResponse) => {
  if (request.type === 'IsEnabled') {
    isEnabled().then(enabled => {
      sendResponse(enabled);
    });
  }

  return true;
});

chrome.browserAction.onClicked.addListener(tab => {
  if (!tab || !tab.id) return;
  chrome.tabs.sendMessage(tab.id, 'toggle', async enabled => {
    // Ignore timing issues due to "document_idle"
    if (typeof enabled !== 'boolean') return;

    setBrowserActionIcon(enabled);
    await storage.set(enabled, 'enabled');
  });
});

async function start() {
  setBrowserActionIcon(await isEnabled());
}

start().catch(err => {
  logger.error(err);
});
