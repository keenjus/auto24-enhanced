import extraInfoLoader from '@/extraInfoLoader';

import {
  start,
  strictQuerySelector,
  formatMileage,
  calculateColor,
  calculateGoodMileage,
  isInjectedElement,
  markElementAsInjected,
  isEnhancedElement,
  markElementAsEnhanced,
  IContentScript,
} from '@/util';

import { ExtraInfoModel } from '@/interfaces';

class MainContentScript implements IContentScript {
  private _mileageRegex = /(\d+)km/;
  private _postIdRegex = /\/used\/(\d+)/;

  private _enabled = false;

  public toggle() {
    if (this._enabled) {
      this.disable();
    } else {
      this.enable();
    }
    return this._enabled;
  }

  public disable() {
    this.getListings().forEach($listing => {
      $listing.style.backgroundColor = '';

      // Reset the extra info processing variables
      $listing.dataset.processing = 'false';
      $listing.dataset.done = 'false';

      // Reset enhancement flag
      markElementAsEnhanced($listing, false);

      const $extraInfo = this.getExtraInfoElement($listing);
      if (!$extraInfo) return;

      const $children = $extraInfo.children;
      for (let i = $children.length - 1; i >= 0; i--) {
        const child = $children[i] as HTMLElement;
        if (!isInjectedElement(child)) continue;
        $extraInfo.removeChild(child);
      }
    });

    this._enabled = false;
  }

  public enable() {
    this.getListings().forEach($listing => {
      if (isEnhancedElement($listing)) return;

      const $extraInfo = this.getExtraInfoElement($listing);
      if (!$extraInfo) return;

      this.registerExtraInfoLoader($listing, $extraInfo);
      markElementAsEnhanced($listing);

      const $yearElement = $listing.querySelector('.year');
      if (!$yearElement) return;

      const year = parseInt($yearElement.innerHTML, 10);
      if (isNaN(year)) return;

      const goodMileage = calculateGoodMileage(year);
      $extraInfo.appendChild(this.createDiv(`eeldatav: ${formatMileage(goodMileage)} km`));

      const found = $extraInfo.innerHTML.replace(/&nbsp;/g, '').match(this._mileageRegex);
      if (!found) return;

      const actualMileage = parseInt(found[1], 10);
      $listing.style.backgroundColor = calculateColor(actualMileage, goodMileage).toString();
    });

    this._enabled = true;
  }

  private registerExtraInfoLoader($resultRow: HTMLTableRowElement, $extraInfo: HTMLElement) {
    const $postIdLink = $resultRow.querySelector<HTMLAnchorElement>('.make_and_model > a');
    if (!$postIdLink) return;
    const matches = $postIdLink.href.match(this._postIdRegex);
    if (!matches) return;
    const postId = matches[1];
    $resultRow.addEventListener('mouseenter', () => this.loadExtraInfo($extraInfo, $resultRow, postId), false);
  }

  private async loadExtraInfo($extraInfo: Element, $row: HTMLTableRowElement, postId: string) {
    if (!this._enabled) return;

    const dataset = $row.dataset;
    if (dataset.done === 'true' || dataset.processing === 'true') {
      return;
    }

    dataset.processing = 'true';

    const extraInfo = await extraInfoLoader.get(postId);

    this.setExtraInfo($extraInfo, extraInfo);
    dataset.processing = 'false';
    dataset.done = 'true';
  }

  private setExtraInfo($extraInfo: Element, extraInfo: ExtraInfoModel) {
    if (extraInfo.originLocation) {
      $extraInfo.appendChild(this.createDiv(`toodud: ${extraInfo.originLocation}`));
    }
    if (extraInfo.currentLocation) {
      $extraInfo.appendChild(this.createDiv(`asukoht: ${extraInfo.currentLocation}`));
    }
  }

  private getExtraInfoElement($listing: HTMLTableRowElement) {
    return $listing.querySelector<HTMLElement>('.make_and_model > .extra');
  }

  private getListings() {
    const $searchResultTable = strictQuerySelector('#usedVehiclesSearchResult');
    return $searchResultTable.querySelectorAll<HTMLTableRowElement>('tbody > tr.result-row');
  }

  private createDiv(text: string) {
    const $element = document.createElement('div');
    markElementAsInjected($element);
    $element.innerText = text;
    return $element;
  }
}

start(new MainContentScript());
