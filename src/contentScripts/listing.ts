import {
  start,
  calculateColor,
  calculateGoodMileage,
  formatMileage,
  markElementAsInjected,
  isEnhancedElement,
  markElementAsEnhanced,
  IContentScript,
} from '@/util';

function getFieldValue(field: string, $content: HTMLDivElement) {
  const $field = $content.querySelector<HTMLSpanElement>(`.field-${field} .value`);
  if (!$field) return null;
  return $field.innerText || null;
}

class ListingContentScript implements IContentScript {
  private _enabled = false;

  public toggle() {
    if (this._enabled) {
      this.disable();
    } else {
      this.enable();
    }
    return this._enabled;
  }

  public disable() {
    const $content = this.getContentElement();
    if (!$content) return;

    markElementAsEnhanced($content, false);

    const $mileage = $content.querySelector<HTMLTableRowElement>('.field-labisoit')!;
    if (!$mileage) return;

    const $mileageValue = $mileage.querySelector<HTMLTableRowElement>('.value');
    if (!$mileageValue) return;

    const $goodMileage = $mileageValue.querySelector('span.injected');
    if (!$goodMileage) return;

    // Reset color
    $mileage.style.backgroundColor = '';
    // Remove good mileage element
    $mileageValue.removeChild($goodMileage);

    this._enabled = false;
  }

  public enable() {
    const $content = this.getContentElement();
    if (!$content) return;

    if (isEnhancedElement($content)) return;

    const yearField = getFieldValue('month_and_year', $content);
    if (!yearField) return;

    const year = this.getYear(yearField);
    if (!year) return;

    const mileageField = getFieldValue('labisoit', $content);
    if (!mileageField) return;

    const goodMileage = calculateGoodMileage(year);
    const mileage = parseInt(mileageField.replace('km', '').replace(/\s/g, ''), 10);
    const color = calculateColor(mileage, goodMileage);
    color.a = 0.2;

    const $mileage = $content.querySelector<HTMLTableRowElement>('.field-labisoit')!;
    const $mileageValue = $mileage.querySelector('.value')!;

    $mileage.style.backgroundColor = color.toString();

    const goodMileageText = document.createTextNode(` (${formatMileage(goodMileage)} km)`);
    const goodMileageWrapper = document.createElement('span');
    markElementAsInjected(goodMileageWrapper);
    goodMileageWrapper.appendChild(goodMileageText);

    $mileageValue.appendChild(goodMileageWrapper);

    markElementAsEnhanced($content);

    this._enabled = true;
  }

  private getYear(yearField: string) {
    const splitValue = yearField.split('/');
    if (splitValue.length === 1) {
      return this.parseYear(splitValue[0]);
    }
    if (splitValue.length === 2) {
      return this.parseYear(splitValue[1]);
    }
    return null;
  }

  private parseYear(year: string) {
    if (year.length === 4) {
      const parsed = parseInt(year, 10);
      return isNaN(parsed) ? null : parsed;
    }
    return null;
  }

  private getContentElement() {
    return document.querySelector<HTMLDivElement>('.tpl-content.have-alt_sidebar');
  }
}

start(new ListingContentScript());
