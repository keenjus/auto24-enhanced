import { IMessage } from './interfaces';
import logger from '@/logger';

class Messaging {
  public request<TResponse, TData>(message: IMessage<TData>) {
    return new Promise<TResponse>((resolve, reject) => {
      try {
        chrome.runtime.sendMessage(message, response => {
          resolve(response);
        });
      } catch (err) {
        logger.error(err);
        reject(err);
      }
    });
  }

  public isExtensionEnabled() {
    return this.request<boolean, unknown>({ type: 'IsEnabled' });
  }
}

export default new Messaging();
