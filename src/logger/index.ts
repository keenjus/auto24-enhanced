interface Options {
  message: string;
  title?: string;
  color?: string;
  colorMessage?: boolean;
}

class Logger {
  public log(message: string, title?: string, titleColor?: string) {
    this._log({
      message,
      title,
      color: titleColor,
    });
  }

  public error(message: string, title?: string) {
    this._log({
      message,
      title,
      color: 'red',
      colorMessage: true,
    });
  }

  private _log(options: Options) {
    const styleArgs = this.getLogStyle(options);
    const message = options.title ? `%c${options.title} %c${options.message}` : `%c${options.message}`;

    console.log(message, ...styleArgs);
  }

  private getLogStyle(options: Options) {
    if (!options.color) return [];

    if (options.title && !options.colorMessage) {
      return [`color: ${options.color}`, 'color: inherit'];
    }

    if (options.colorMessage) {
      return [`color: ${options.color}`];
    }

    return [];
  }
}

export default new Logger();
