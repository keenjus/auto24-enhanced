import RGBa from './RGBa';

export { RGBa };
export * from './dom';
export * from './contentScript';

export function limitNumber(n: number) {
  if (n > 100) {
    return 100;
  } else if (n < 0) {
    return 0;
  }

  return n;
}

export function formatMileage(n: number) {
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}

export function calculateGoodMileage(year: number) {
  return (new Date().getFullYear() - year) * 20000;
}

export function calculateColor(actualMileage: number, estimatedMileage: number) {
  const calculated = limitNumber((75 * actualMileage) / (estimatedMileage / 0.5));

  const red = Math.round((255 * calculated) / 100);
  const green = Math.round((255 * (100 - calculated)) / 100);

  return new RGBa(red, green, 0, 0.2);
}
