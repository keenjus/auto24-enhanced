export function strictQuerySelector<T extends Element>(selector: string) {
  const element = document.querySelector<T>(selector);
  if (!element) throw new Error('Element not found');
  return element;
}

export function isInjectedElement(element: HTMLElement) {
  return element.classList.contains('injected');
}

export function markElementAsInjected(element: HTMLElement) {
  element.classList.add('injected');
}

export function isEnhancedElement(element: HTMLElement) {
  return element.dataset.enhanced === 'true';
}

export function markElementAsEnhanced(element: HTMLElement, enhanced: boolean = true) {
  element.dataset.enhanced = enhanced ? 'true' : 'false';
}
