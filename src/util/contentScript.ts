import logger from '@/logger';
import messaging from '@/messaging';

export interface IContentScript {
  enable(): void;
  disable(): void;
  toggle(): boolean;
}

export async function bootstrap(cs: IContentScript) {
  const isEnabled = await messaging.isExtensionEnabled();
  if (isEnabled) {
    cs.enable();
  }

  chrome.runtime.onMessage.addListener((request, _, sendResponse) => {
    if (request !== 'toggle') return;
    sendResponse(cs.toggle());
  });
}

export async function start(cs: IContentScript) {
  try {
    return bootstrap(cs);
  } catch (err) {
    logger.error(err);
  }
}
