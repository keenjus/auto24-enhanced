const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader')

const projectRoot = path.join(__dirname, '../');

const sourceRootPath = path.join(projectRoot, '/src');
const distRootPath = path.join(projectRoot, '/dist');

module.exports = {
  entry: {
    background: path.join(sourceRootPath, 'background.ts'),
    'contentScripts/listing': path.join(sourceRootPath, 'contentScripts/listing.ts'),
    'contentScripts/main': path.join(sourceRootPath, 'contentScripts/main.ts'),
  },
  output: {
    path: distRootPath,
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [
      new TsConfigPathsPlugin(),
    ],
  },
  plugins: [
    new CheckerPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.join(sourceRootPath, 'assets'),
        to: path.join(distRootPath, 'assets'),
        test: /\.(jpg|jpeg|png|gif|svg)?$/,
      },
      {
        from: path.join(sourceRootPath, 'manifest.json'),
        to: path.join(distRootPath, 'manifest.json'),
        toType: 'file',
      },
    ]),
    new MomentLocalesPlugin(),
  ]
};
