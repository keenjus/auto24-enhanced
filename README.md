# Auto24Enhanced
A web extension for enhancing [auto24.ee](http://www.auto24.ee/)

![Example](screenshots/example2.gif "Example")

# Getting started

## Requirements
* nodejs 10+
* yarn

## Building
```sh
# Install dependencies
yarn install

# Build & copy files
yarn run build

# Start web-ext debugging
yarn run serve

# Compile and package for production with web-ext
yarn run package
```
